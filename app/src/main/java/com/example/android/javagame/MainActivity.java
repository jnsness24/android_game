package com.example.android.javagame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.android.javagame.R;

import com.example.android.javagame.GameActivity;

public class MainActivity extends Activity
        implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set UI Layout
        setContentView(R.layout.activity_main);

        // Reference to the button in UI
        final Button buttonPlay =
                (Button)findViewById(R.id.buttonPlay);

        buttonPlay.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, GameActivity.class);
        // Start GameActivity class via the Intent
        startActivity(i);
        finish();
    }
}