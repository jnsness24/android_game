package com.example.android.javagame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

/**
 * Created by Jnsness on 23.10.2016.
 */

public class Asteroid {
    private Bitmap bitmap;
    private int locationX, locationY;
    private int speed = 1;

    private int maxLocationX;
    private int minLocationX;

    private int maxLocationY;

    private Rect hitBox;

    public Asteroid(Context context, int screenX, int screenY) {

        BitmapFactory.Options scaleOptions = new BitmapFactory.Options();
        scaleOptions.inSampleSize = 7;
        bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.asteroid,scaleOptions);
        maxLocationX = screenX;
        maxLocationY = screenY;
        minLocationX = 0;

        Random generator = new Random();

        //generator.nextint(6) returns a random number between 0 and 5 - so speed will be
        //between 10 and 15
        speed = generator.nextInt(6)+10;

        locationX = screenX;
        locationY = generator.nextInt(maxLocationY) - bitmap.getHeight();

        hitBox = new Rect(locationX, locationY, bitmap.getWidth(), bitmap.getHeight());
    }

    public void calculateSpeed(int playerSpeed) {
        locationX -= playerSpeed;
        locationX -= speed;

        //set the asteroid to a random location on the right side if he passes by
        if (locationX < minLocationX - bitmap.getWidth()) {
            Random generator = new Random();
            speed = generator.nextInt(10) + 10;
            locationX = maxLocationX;
            locationY = generator.nextInt(maxLocationY) - bitmap.getHeight();
        }
    }

    public void calculateHitBox() {
        hitBox.left = locationX;
        hitBox.top = locationY;
        hitBox.right = locationX + bitmap.getWidth();
        hitBox.bottom = locationY + bitmap.getHeight();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getLocationX() {
        return locationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public Rect getHitbox(){
        return hitBox;
    }
}
