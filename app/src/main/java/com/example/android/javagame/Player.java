package com.example.android.javagame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

/**
 * Created by Jnsness on 13.10.2016.
 */

public class Player {

    private boolean flyUp;

    private Bitmap bitmap;
    private int locationX, locationY;
    private int speed = 0;

    //this will stop the ship leaving the screen
    private int maxLocationX, maxLocationY;

    private Rect hitBox;

    public Player(Context context, int screenY) {
        locationX = 50;
        locationY = 50;
        speed = 5;

        //rescale the Image of the ship - make it bigger
        BitmapFactory.Options scaleOptions = new BitmapFactory.Options();
        scaleOptions.inSampleSize = 6;
        bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.ship,scaleOptions);

        maxLocationX = screenY - bitmap.getHeight();
        maxLocationY = 0;

        hitBox = new Rect(locationX, locationY, bitmap.getWidth(), bitmap.getHeight());
    }

    public void calculateSpeed() {

        if(flyUp) {
            locationY += speed;
        }
        else {
            locationY -= speed;
        }

        if (locationY < maxLocationY) {
            locationY = maxLocationY;
        }

        if (locationY > maxLocationX) {
            locationY = maxLocationX;
        }

    }

    public void calculateHitBox() {
        hitBox.left = locationX;
        hitBox.top = locationY;
        hitBox.right = locationX + bitmap.getWidth();
        hitBox.bottom = locationY + bitmap.getHeight();
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getLocationX() {
        return locationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public void setLocationY(int locationY) {
        this.locationY = locationY;
    }

    public int getSpeed() {
        return speed;
    }

    public Rect getHitbox(){
        return hitBox;
    }

    public void calculateFlyingDirection(float locationTouchY) {
        if (locationY < locationTouchY) {
            flyUp = true;
        }
        else {
            flyUp = false;
        }

    }

}
