package com.example.android.javagame;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

public class GameActivity extends Activity {

    private GameController gameController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // get the screen resolution from current Smartphone/Tablet
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        //hand over the display size to the View to handle with it

        gameController = new GameController(this, size.x, size.y);
        setContentView(gameController);
    }

    protected void onPause() {
        super.onPause();
        gameController.pause();
    }

    protected void onResume() {
        super.onResume();
        gameController.resume();
    }
}
