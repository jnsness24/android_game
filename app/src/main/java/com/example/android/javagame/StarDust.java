package com.example.android.javagame;

import java.util.Random;

/**
 * Created by Jnsness on 23.10.2016.
 */

public class StarDust {

    private int locationX, locationY;
    private int speed;
    private int maxLocationX, maxLocationY;

    public StarDust(int screenX, int screenY) {
        maxLocationX = screenX;
        maxLocationY = screenY;

        Random generator = new Random();
        speed = generator.nextInt(10);

        locationX = generator.nextInt(maxLocationX);
        locationY = generator.nextInt(maxLocationY);
    }

    public void calculateSpeed(int playerSpeed) {
        locationX -= playerSpeed;
        locationX -= speed;

        if(locationX < 0) {
            locationX = maxLocationX;
            Random generator = new Random();
            locationY = generator.nextInt(maxLocationY);
            speed = generator.nextInt(15);
        }
    }

    public int getLocationX() {
        return locationX;
    }

    public int getLocationY() {
        return locationY;
    }
}
