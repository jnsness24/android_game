package com.example.android.javagame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Jnsness on 12.10.2016.
 */


public class GameController extends SurfaceView implements Runnable {


    volatile boolean playing;
    Thread gameThread = null;
    private Player player;
    private Asteroid asteroid1;
    private Asteroid asteroid2;
    private Asteroid asteroid3;
    private ArrayList<StarDust> starDustList = new ArrayList<StarDust>();

    private int maxScreenY;
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder myHolder;
    private static Logger LOGGER = Logger.getLogger("ExceptionLogger in " + GameController.class.getName());


    public GameController(Context context, int x, int y) {
        super(context);
        myHolder = getHolder();
        paint = new Paint();
        this.maxScreenY = y;
        player = new Player(context, y);
        asteroid1 = new Asteroid(context, x, y);
        asteroid2 = new Asteroid(context, x, y);
        asteroid3 = new Asteroid(context, x, y);

        int numSpecs = 40;
        for (int i = 0; i < numSpecs; i++) {
            StarDust spec = new StarDust(x, y);
            starDustList.add(spec);
        }
    }


    @Override
    public void run() {
        while (playing) {
            update();
            draw();
            control();
        }
    }

    private void update() {
        player.calculateSpeed();
        player.calculateHitBox();

        asteroid1.calculateSpeed(player.getSpeed());
        asteroid1.calculateHitBox();
        asteroid2.calculateSpeed(player.getSpeed());
        asteroid2.calculateHitBox();
        asteroid3.calculateSpeed(player.getSpeed());
        asteroid3.calculateHitBox();

        for (StarDust sd : starDustList) {
            sd.calculateSpeed(player.getSpeed());
        }

        //checks if player collides with one of the asteroids
        if (Rect.intersects(player.getHitbox(), asteroid1.getHitbox()) || Rect.intersects(player.getHitbox(), asteroid2.getHitbox()) || Rect.intersects(player.getHitbox(), asteroid3.getHitbox())) {
            pauseAndSetNewLocation();
        }
    }

    private void draw() {
        if (myHolder.getSurface().isValid()) {
            canvas = myHolder.lockCanvas(); //make sure canvas is available to be written
            canvas.drawColor(Color.argb(255, 0, 0, 0));

            // White specs of Stardust
            paint.setColor(Color.argb(255, 255, 255, 255));
            for (StarDust sd : starDustList) {
                canvas.drawPoint(sd.getLocationX(), sd.getLocationY(), paint);
            }

            canvas.drawBitmap(player.getBitmap(), player.getLocationX(), player.getLocationY(), paint);
            canvas.drawBitmap(asteroid1.getBitmap(), asteroid1.getLocationX(), asteroid1.getLocationY(), paint);
            canvas.drawBitmap(asteroid2.getBitmap(), asteroid2.getLocationX(), asteroid2.getLocationY(), paint);
            canvas.drawBitmap(asteroid3.getBitmap(), asteroid3.getLocationX(), asteroid3.getLocationY(), paint);

            myHolder.unlockCanvasAndPost(canvas);
        }

    }

    private void control() {
        try {
            //(1000(milliseconds)/60(FPS)) to slow down Thread - otherwise you can't see anything
            gameThread.sleep(17);

        } catch (InterruptedException ie) {
            LOGGER.log(Level.SEVERE, ie.toString());
            gameThread.currentThread().interrupt();
        }

    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();

        } catch (InterruptedException ie) {
            LOGGER.log(Level.SEVERE, ie.toString());
            gameThread.currentThread().interrupt();
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    private void pauseAndSetNewLocation() {
        try {
            gameThread.sleep(2000);
        } catch (InterruptedException ie) {
            LOGGER.log(Level.SEVERE, ie.toString());
            gameThread.currentThread().interrupt();
        }
        Random generator = new Random();
        // sets the location from player to a random Y value (from 0 to the max height of screen)
        player.setLocationY(generator.nextInt(maxScreenY));

    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        player.calculateFlyingDirection(motionEvent.getY());
        return true;
    }
}
